<?PHP
//   include("Include/config.php");

  class DBManagement {
    // public $dbSettings = new object();
    public $db = '';
    public $server = '';
    public $dbUser = '';
    public $dbPwd = '';
    public $idConnection = '';
    public $idConsult = '';
    public $errorText = '';
    public $errorNum = '';
    
    public function DBManagement($db='', $host='', $user='', $pwd='') {
        $this->dbSettings->db = $db;
        $this->dbSettings->server = $host;
        $this->dbSettings->user = $user;
        $this->dbSettings->pwd = $pwd;
        
    }
    
    public function connect($dbSettings) {
        $this->dbSettings->db = $dbSettings->$db;
        $this->dbSettings->server = $dbSettings->$host;
        $this->dbSettings->user = $dbSettings->$user;
        $this->dbSettings->pwd = $dbSettings->$pwd;
        $this->dbSettings->idConnection = mysqli_connect($this->dbSettings->server, $this->dbSettings->user, $this->$dbSettings->pwd);
        echo 'idCon: ' . $this->dbSettings->idConnection;
        if (!$this->dbSettings->idConnection) {
            $this->dbSettings->errorText = 'Connection failed: ';
            $this->dbSettings->errorNum = mysqli_errno();
            return false;
        }
        if(!mysql_select_db($this->dbSettings->db, $this->dbSettings->idConnection)){
            $this->dbSettings->errorText = 'No connection with the Database';
            $this->dbSettings->errorNum = mysqli_errno();
            return false;
        }
        echo 'idCon: ' . $this->dbSettings->idConnection;
        return $this->dbSettings->idConnection;
    }

    public function useDB($db) {
        $this->dbSettings->db;
        if(!$this->dbSettings->idConnection) {
            $this->dbSettings->errorText = 'Cannot open: ' . $this->dbSettings->db;
            return false;
        }
        return true;
    }

    public function select($sql=''){
        $e=$this->existQuery($sql);
        if($e>0){
           $this->dbSettings->idConsult = mysql_query($sql, $this->dbSettings->idConnection);
           if(!$this->dbSettings->idConnection){
            $this->dbSettings->errorNum=mysql_errno();
            $this->dbSettings->errorText=mysql_error();
           }
           return $this->dbSettings->idConsult; 
        }
        return $e;  // return 0 if there are errors
    }

    public function insert($sql=''){
        return $this->select($sql);
    }
  
    public function update($sql=""){
        return $this->select($sql);
    }
  
    public function delete ($sql=""){
      $e=$this->existQuery($sql);
      if($e>0){
         $e = mysql_query($sql,$this->dbSettings->idConnection);
         if(!$e){
            $this->dbSettings->errorNum=mysql_errno();
            $this->dbSettings->errorText=mysql_error();
         }
      }
      return $e; // return 0 if there are errors
    }

    // return the total of rows
    public function getRow($idConsult){
        if($idConsult<0){  
        if($row= mysql_fetch_array($this->dbSettings->idConsulta))
            return $row;
        else
            return false;
        } else {
        if($row= mysql_fetch_array($idConsult))
            return $row;
        else
            return false;   
        }
    }

    public function countRows($idConsult){
        if($idConsult<0)
            return mysql_num_rows($this->dbSettings->idConsult); // return the number of rows from a consult
        else
            return mysql_num_rows($idConsult);
    }
    
    // return the last max id (rows total from table)
    function insertId(){
       return mysql_insert_id();
    }
}
<?php 
$RootFolder = $_SERVER['DOCUMENT_ROOT'] . '/Custom_CMS';
include_once $RootFolder . '/Include/DB.php';
include_once $RootFolder . '/Include/Sessions.php';
include_once $RootFolder . '/Include/Functions.php';

$currentUser = $_SESSION['User'];
$name = $_POST['firstname'];
$lastname = $_POST['lastname'];
$phone = $_POST['phone'];
$errorMsg = "";

if(!empty($name)&&!empty($lastname)&&!empty($phone)){
    if((preg_match("/^[A-Za-z\. ]*$/", $name)==true)&&(preg_match("/^[A-Za-z\. ]*$/", $lastname)==true)&&(preg_match("/[0-9]/", $phone)==true)){
        $success = "UPDATE users SET first_name='$name', last_name='$lastname', phone='$phone' WHERE username = '$currentUser'";
        $Execute = mysqli_query($Connection, $success);

        //Success or error message
        if ($success && $errorMsg == ""){
            echo "Profile Succesfully Updated";
        }
        else{
            if($errorMsg == ""){
                echo "Something went wrong";
            } else {
                echo $errorMsg;
            }
        }
    }
}
else {
    if(empty($name)){
        $errorMsg = "First Name is required";
    }
    if(empty($lastname)){
        $errorMsg .= "Last Name is required";
    }
    if(empty($phone)){
        $errorMsg .= "Phone is required";
    }
    echo $errorMsg;
}

?>
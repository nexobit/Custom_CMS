<?php
  include("Include/config.php");
  
  class PagesController {
    
    function PagesController($conf){
      $this->conf=$conf;
      require_once($this->conf["pathModel"]."Model.php");
      $pathGui = $this->conf["pathGUI"];
		  $this->objModel= new PagesModel($this->conf);
    }

    public function home() {
      $first_name = 'ns';
      $last_name  = 'nesamaju';
      require_once($this->conf["pathGUI"] . 'home.php');
    }

    public function error() {
      require_once($this->conf["pathGUI"] . 'error.php');
    }

    public function profile() {
      $profile = $this->objModel->GetProfile();
      require_once($this->conf["pathGUI"] . 'profile.php');
    }
  }
?>
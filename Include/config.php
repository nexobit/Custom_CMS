<?PHP
    $conf["pathGUI"]='views/';
    $conf["pathModel"]='Models/';
    $conf["pathControl"]='Controllers/';
    $conf["pathInclude"]='Include/';
    $conf["pathdb"]='Include/db/';   
    $conf["pathContent"]='Content/';
    $conf["pathScripts"]='Scripts/';

    //section for all resources
    $cssCore = $conf['pathContent'].'Styles/framework/bulma.css';
    $cssBase = $conf['pathContent'].'Styles/css.css';
    $fontBase = $conf['pathContent'].'Styles/font-awesome.css';
    $imgLogo = $conf['pathContent'].'Images/logos/logo.png';
    $profilePic = $conf['pathContent'].'Images/profile-pic.jpg';
    $jsCore = $conf['pathScripts'].'framework/jquery/jquery_v3.2.1.js';
    $jsUICore = $conf['pathScripts'].'framework/bootstrap/bootstrap.js';
    $jsCustomCms = $conf['pathScripts'].'custom-cms.js';
?>
<?php 
    // $RootFolder = $_SERVER['DOCUMENT_ROOT'] . '/Custom_CMS';
    // require_once $RootFolder . '/Include/DB.php';
    // require_once $RootFolder . '/Include/Sessions.php';

    function Redirect_to($New_Location){
        header("Location:" . $New_Location);
        exit;
    }

    function Login() {
        if(isset($_SESSION['User'])){
            return true;
        }
    }

    function Confirm_Login() {
        if(!Login()){
            Redirect_to('./views/login.php');// Corregir URL Absoluto
        }
    }
?>
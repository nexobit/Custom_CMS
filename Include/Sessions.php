<?php
    // session_start();

    function ErrorMessage(){
        if(isset($_SESSION["ErrorMessage"])){
            $Output = "<div class=\"notification is-danger\">";
            $Output.= "<button class=\"delete\"></button>";
            $Output.= "<i class=\"fa fa-exclamation-triangle\"></i>";
            $Output.= "<span>" . htmlentities($_SESSION["ErrorMessage"]) . "</span>";
            $Output.= "</div>";

            $_SESSION["ErrorMessage"] = null;
            return $Output;
        }
    }

    $_SESSION['User'] = htmlentities($_SESSION["User"]);
?>

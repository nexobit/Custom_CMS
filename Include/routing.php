
<?php
    // session_start();
    include("config.php");

  function call($path, $controller, $action) {
    // require the file that matches the controller name
    require_once($path . $controller . '-controller.php');

    // create a new instance of the needed controller
    switch($controller) {
      case 'main':
        $controller = new PagesController($conf);
      break;
    }

    // call the action
    $controller->{ $action }();
  }

  // just a list of the controllers we have and their actions
  // we consider those "allowed" values
  $controllers = array('main' => ['home', 'error', 'profile']);

  // check that the requested controller and action are both allowed
  // if someone tries to access something else he will be redirected to the error action of the pages controller
  if (array_key_exists($controller, $controllers)) {
    $path = $conf['pathControl'];
    echo 'action ' . in_array($action, $controllers[$controller]);
    if (in_array($action, $controllers[$controller])) {
        
      call($path, $controller, $action);
    } else {
      call($path, 'main', 'error');
    }
  } else {
    call($path, 'main', 'error');
  }
?>
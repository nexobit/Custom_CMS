
<?php 
	session_start();
	include("Include/config.php");
	require_once($conf["pathdb"]."db.php");
	require_once($conf["pathInclude"]."Sessions.php");
	require_once($conf["pathInclude"]."Functions.php");
	Confirm_Login();
	$currentUser = $_SESSION['User'];

	if (isset($_GET['controller']) && isset($_GET['action'])) {
		$controller = $_GET['controller'];
		$action     = $_GET['action'];
	  } else {
		$controller = 'main';
		$action     = 'home';
	  }

	require_once ($conf["pathGUI"]."shared/_Layout.php");
	require_once ("views/shared/footer.php"); 
?>

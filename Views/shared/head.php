
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Nexobit CMS</title>
        <meta name="description" contents="Just a test website for learning html, css and php">

        <!-- Bootstrap/Bulma Core CSS -->
        <link href="<?PHP echo $cssCore; ?>" rel="stylesheet" type="text/css">

        <!-- Custom Theme CSS -->
        <link href="<?PHP echo $cssBase; ?>" rel="stylesheet" type="text/css">

        <!-- Custom Fonts -->
        <link href="<?PHP echo $fontBase; ?>" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="<?PHP echo $jsCore; ?>"></script>
    </head>
    <body>
        <header>
            <div class="header-container">
                <div class="navbar-header">
                    <a href="http://www.nexobit.io" class="logo-link"></a>
                    <img class="logo" src="<?PHP echo $imgLogo; ?>" alt="logo" />
                    <span>Nexobit</span>
                </div>
                <div class="session-menu">
                    <a href="index.php?controller=main&action=profile" class="profile">
                        <div class="img-container">
                            <img src="<?PHP echo $profilePic; ?>" alt="profile" />
                        </div>
                        <span><?php echo $currentUser ?></span>
                    </a>
                </div>
            </div>
        </header>
        <aside class="admin-panel">
            <div class="menu-container">
                <nav class="sidebar-nav">
                    <ul class="menu-list">
                        <li class="active">
                            <a href="/Custom_CMS/">Dashboard</a>
                        </li>
                        <li>
                            <a href="#">Portfolio</a>
                        </li>
                        <li>
                            <a href="#">Clients</a>
                        </li>
                        <li>
                            <a href="#">About Us</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="aside-footer">
                <a href="/Custom_CMS/views/logout.php">
                    <i class="fa fa-power-off"></i>
                    <span>Log Out</span>
                </a>
            </div>
        </aside>

        
        
        <div class="page-wrapper">
            <div class="container is-fluid">
                <?PHP require_once $conf['pathInclude'].'routing.php'; ?>
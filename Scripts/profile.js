//First Name Input Selector
var controlGroupName = $('#firstName').parents(".field").first(),
    helpBlockName = controlGroupName.find(".help").first();
//Last Name Input Selector
var controlGroupLastName = $('#lastName').parents(".field").first(),
    helpBlockLastName = controlGroupLastName.find(".help").first();
//Phone Input Selector
var controlGroupPhone = $('#phone').parents(".field").first(),
    helpBlockPhone = controlGroupPhone.find(".help").first();

// Image Preview
var loadFile = function(event) {
    // Preview Image
    var output = document.getElementById('profilePicture');
    output.src = URL.createObjectURL(event.target.files[0]);
    // Bulma File Input UI
    if(file.files.length > 0)
    {
      document.getElementById('filename').innerHTML = file.files[0].name;
    }
};

// Name Preview
$('#firstName').on('input',function(e){
    $(".title").text($(this).val());
    $(this).parents(".field").removeClass('error');

    validateName();
});

// Last Name Preview
$('#lastName').on('input',function(e){
    $(".subtitle").text($(this).val());
    $(this).parents(".field").removeClass('error');

    validateLastName();
});

// Phone Preview
$('#phone').on('input',function(e){
    $(".phone").text($(this).val());
    $(this).parents(".field").removeClass('error');

    validatePhone();
});

//Click on Submit Button
$("#submit").on("click",function(e){
    e.preventDefault();
    submitForm();
});

$("#cancel").on('click', function(e) {
    e.preventDefault();
    window.location.href = '/Custom_CMS/index.php';
  });

//Regular Expressions
var nameReg = new RegExp(/^[A-Za-z\. ]*$/),
    lastnameReg = new RegExp(/^[A-Za-z\. ]*$/),
    phoneReg = new RegExp(/^[0-9]*$/);

function submitForm(){
    //Initiate Variables With Form Content
    var name = $("#firstName").val(),
        lastname = $("#lastName").val(),
        phone = $("#phone").val();

    //Check if inputs have information
    if (name!='' && lastname!='' && phone!=''){
        if(nameReg.test(name)&&lastnameReg.test(lastname)&&phoneReg.test(phone)){
            $.ajax({
                url: "/Custom_CMS/views/controllers/profile_submit.php",
                type: "POST",
                data: $('form').serialize(),
                success:
                function(text){
                    if (text == "Profile Succesfully Updated"){
                        submitMsg(true, text);
                    }
                    else {
                        submitMsg(false,text);
                    }
                }
            });
        }
        else {
            validateInputs();
        }
    }
    else {
        validateInputs();
    }
}

// Validations
function validateInputs() {
    $('.field').removeClass('error');
    validateName();
    validateLastName();
    validatePhone()
}

function validateName() {
    // Name empty validation
    if(!$('#firstName').val()){
        $(controlGroupName).addClass('error');
        $(helpBlockName).html('Please enter your name');
    }
    else{
        if(!nameReg.test($('#firstName').val())){
            $(controlGroupName).addClass('error');
            $(helpBlockName).html('<span>Only text are available</span>');
        }
    }
}

function validateLastName(){
    // Last Name empty validation
    if(!$('#lastName').val()){
        $(controlGroupLastName).addClass('error');
        $(helpBlockLastName).html('<span>Please enter your last name</span>');
    }
    else{
        if(!lastnameReg.test($('#lastName').val())){
            $(controlGroupLastName).addClass('error');
            $(helpBlockLastName).html('<span>Only text are available</span>');
        }
    }
}

function validatePhone(){
    // Phone empty validation
    if(!$('#phone').val()){
        $(controlGroupPhone).addClass('error');
        $(helpBlockPhone).html('<span>Please enter your phone</span>');
    }
    else{
        if(!phoneReg.test($('#phone').val())){
            $(controlGroupPhone).addClass('error');
            $(helpBlockPhone).html('<span>Enter a valid phone format</span>');
        }
    }
}

// Error/Success message clases
function submitMsg(valid, msg){
    var msgClasses;
    var html = "<i class='fa fa-exclamation-triangle'></i>"+
               "<span>" + msg + "</span>";
    if(valid){
        msgClasses = "notification is-success";
    }
    else {
        msgClasses = "notification is-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).html(html);
}
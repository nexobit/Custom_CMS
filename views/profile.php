<?php 
// session_start();
    // include("../Include/config.php");
	// require_once($this->conf["pathdb"]."db.php");
	// require_once($conf["pathInclude"]."Sessions.php");
	// require_once($conf["pathInclude"]."Functions.php");
	// Confirm_Login();
    $currentUser = $_SESSION['User'];
    //$Connection = 'not connection';
    
?>
<div class="profile" style="padding-top: 20px;">
    <div class="columns">
        <div class="column is-one-third">
            <div class="card">
                <div class="card-content">
                    <img class="profile-bg" src="https://image.freepik.com/free-psd/abstract-background-design_1297-87.jpg" alt="background image">
                    <figure>
                        <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image" id="profilePicture">
                    </figure>
                    <div class="user-info">
                        <div class="media">
                            <div class="media-content">
                                <p class="title is-4"><?php echo $name ?></p>
                                <p class="subtitle is-6"><?php echo $lastname ?></p>
                            </div>
                        </div>
                        <span class="phone"><?php echo $phone ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="column">
            <form id="profileForm" class="edition-panel" novalidate action="" method="post">
            <div id="msgSubmit" class="hidden"></div>
                <div class="field">
                    <label class="label">Profile Picture</label>
                    <div class="file has-name is-fullwidth">
                        <label class="file-label">
                            <input class="file-input" type="file" name="resume" id="file" onchange="loadFile(event)">
                            <span class="file-cta">
                            <span class="file-icon">
                                <i class="fa fa-upload"></i>
                            </span>
                            <span class="file-label">Choose a file…</span>
                            </span>
                            <span class="file-name" id="filename"></span>
                        </label>
                    </div>
                </div>
                <div class="field">
                    <label class="label">First Name</label>
                    <div class="control">
                        <input class="input" type="text" placeholder="First Name" name="firstname" id="firstName" value="<?php echo $name ?>" />
                        <p class="help is-danger"></p>
                    </div>
                </div>
                <div class="field">
                    <label class="label">Last Name</label>
                    <div class="control">
                        <input class="input" type="text" placeholder="Last Name" name="lastname" id="lastName" value="<?php echo $lastname ?>" />
                        <p class="help is-danger"></p>
                    </div>
                </div>
                <div class="field">
                    <label class="label">Phone</label>
                    <div class="control">
                        <input class="input" type="text" placeholder="Phone Number" name="phone" id="phone" value="<?php echo $phone ?>" />
                        <p class="help is-danger"></p>
                    </div>
                </div>
                <button class="button is-danger" name="Cancel" id="cancel">Cancel</button>
                <button class="button is-primary" name="Submit" id="submit">Save</button>
            </form>
        </div>
    </div>
</div>

<!-- Profile -->
<script src="/Custom_CMS/Scripts/profile.js"></script>

<?php include_once ("shared/footer.php"); ?>

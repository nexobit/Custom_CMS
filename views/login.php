<?php 
    // session_start();
    $RootFolder = $_SERVER['DOCUMENT_ROOT'] . '/Custom_CMS';
        // require_once $RootFolder . '/Include/DB.php';
        // require_once $RootFolder . '/Include/Sessions.php';
        // require_once $RootFolder . '/Include/Functions.php';

    // include("../include/config.php");
    // require_once($conf["pathdb"]."db.php");
    // require_once($conf["pathInclude"]."Sessions.php");
    // require_once($conf["pathInclude"]."Functions.php");
    
    if(isset($_POST["Submit"])){

        $Username = $_POST['Username'];
        $Password = $_POST['Password'];

        // prevent sql injection
        $Username = stripslashes($Username);
        $Password = stripslashes($Password);
        $Username = mysqli_real_escape_string($Connection, $_POST["Username"]);
        $Password = mysqli_real_escape_string($Connection, $_POST["Password"]);

        if(empty($Username)||empty($Password)){
            $_SESSION["ErrorMessage"] = "All Fields must be filled out";
            Redirect_to("/Custom_CMS/views/login.php");
        }

        else{
            $Selected;
            $Query = "SELECT * FROM users WHERE username ='$Username' AND password ='$Password'";
            $Result = mysqli_query($Connection, $Query);
            $row = mysqli_fetch_array($Result);

            if($row['username'] == $Username && $row['password'] == $Password){
                $_SESSION['User'] = $row['username'];
                Redirect_to("/Custom_CMS/index.php");
            }
            else {
                $_SESSION["ErrorMessage"] = "Invalid Username / Password";
                Redirect_to("/Custom_CMS/views/login.php");
            }
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>

        <!-- Bootstrap Core CSS -->
        <link href="/Custom_CMS/Content/Styles/framework/bulma.css" rel="stylesheet" type="text/css">

        <!-- Custom Fonts -->
        <link href="/Custom_CMS/Content/Styles/font-awesome.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="login-container">
            <form method="post" action="login.php" class="login_form">
                <?php echo ErrorMessage(); ?>
                <div class="field">
                  <p class="control has-icons-left">
                    <input class="input" type="text" placeholder="Username" name="Username" id="Username">
                    <span class="icon is-small is-left">
                      <i class="fa fa-user"></i>
                    </span>
                  </p>
                </div>
                <div class="field">
                  <p class="control has-icons-left">
                    <input class="input" type="password" name="Password" id="Password" placeholder="Password">
                    <span class="icon is-small is-left">
                      <i class="fa fa-lock"></i>
                    </span>
                  </p>
                </div>
                <input  class="button is-primary" type="submit" name="Submit" value="Login"/>
            </form>
        </div>
    </body>
</html>

<?php 
    $RootFolder = $_SERVER['DOCUMENT_ROOT'] . '/Custom_CMS';
    require_once $RootFolder . '/Include/DB.php';
    require_once $RootFolder . '/Include/Sessions.php';
    require_once $RootFolder . '/Include/Functions.php';

    $_SESSION['User'] = null;
    session_destroy();
    Redirect_to('login.php');
?>